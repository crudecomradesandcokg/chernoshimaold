from a_star import AStar
import pygame
import RPi.GPIO as GPIO
from time import sleep

   

def motors_speed(a_star,speed1, speed2):
    a_star.motors(speed1,speed2)

def SetAngle(angle):
    print("setangle")
    duty = (pwm_MAX*angle/180)+pwm_MIN
    if angle == 55: #rechtes quadrat fährt hoch
##        x = x-0.01
##        if x>= pwm_MIN:
            pwm.ChangeDutyCycle(2.5)
        
    elif angle == 120: #linkes O fährt runter
##        x = x+ 0.01
##        if x <= pwm_MAX:
            pwm.ChangeDutyCycle(7.5)
        

    
    #duty = angle/18+2
    #GPIO.output(18,True)
    #pwm.ChangeDutyCycle(duty)
    #sleep(1)
    #GPIO.output(18, False)
    #pwm.ChangeDutyCycle(0)                


def getAxis(number):
    # when nothing is moved on an axis, the VALUE IS NOT EXACTLY ZERO
    # so this is used not "if joystick value not zero"
    if joystick.get_axis(number) < -0.1 or joystick.get_axis(number) > 0.1:
      # value between 1.0 and -1.0
      print ("Axis value is %s" %(joystick.get_axis(number)))
      print ("Axis ID is %s" %(number))
 
def getButton(number):
    # returns 1 or 0 - pressed or not
    if joystick.get_button(number):
      # just prints id of button
      print ("Button ID is %s" %(number))

def getHat(number):
    if joystick.get_hat(number) != (0,0):
      # returns tuple with values either 1, 0 or -1
      print ("Hat value is %s, %s" %(joystick.get_hat(number)[0],joystick.get_hat(number)[1]))
      print ("Hat ID is %s" %(number))

GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.OUT)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.output(23, GPIO.LOW)
GPIO.output(17, GPIO.LOW)

pwm = GPIO.PWM(18,50)
pwm.start(2.5)

#2.5 weil 2.5% für 0 Grad entspricht
#pwm.start(2.5)

pwm_MAX = 7.5
pwm_MIN = 2.5
x=2.5
setze = True


stick_oder_steuerkreuz = False

a_star = AStar()


# setup the pygame
pygame.init()
pygame.joystick.init()

# how many joysticks connected to computer?
joystick_count = pygame.joystick.get_count()
print ("There is " + str(joystick_count) + " joystick/s")

if joystick_count == 0:
    # if no joysticks, quit program safely
    print ("Error, I did not find any joysticks")
    print("Shit was happening")
    pwm.stop()
    GPIO.cleanup()
    pygame.quit()
    sys.exit()
else:
    # initialise joystick
    joystick = pygame.joystick.Joystick(0)
    joystick.init()

axes = joystick.get_numaxes()
buttons = joystick.get_numbuttons()
hats = joystick.get_numhats()

print ("There is " + str(axes) + " axes")
print ("There is " + str(buttons) + " button/s")
print ("There is " + str(hats) + " hat/s")


try:
    while True:
        for event in pygame.event.get():
            print ("event is %s" %(event))
            if stick_oder_steuerkreuz == False:
                    left_stick = float(joystick.get_axis(1))
                    right_stick = float(joystick.get_axis(3))
                    a_star.motors(int(-300*left_stick),int(300*right_stick))
            if event.type==pygame.JOYBUTTONDOWN:
                print("Button pressed")
                if event.button == 4 and stick_oder_steuerkreuz == True:  #if joystick.get_button(13):
                    #buttons_up(a_star)
                    motors_speed(a_star,200,-200)
                if event.button == 7 and stick_oder_steuerkreuz == True: #if joystick.get_button(15):
                    #buttons_left(a_star)
                    motors_speed(a_star,-200,-200)
                if event.button == 5 and stick_oder_steuerkreuz == True: #if joystick.get_button(16):
                    motors_speed(a_star,200,200)
                if event.button == 6 and stick_oder_steuerkreuz == True:
                    motors_speed(a_star,-200,200)
                if event.button == 0:
                    stick_oder_steuerkreuz = not stick_oder_steuerkreuz #toggle mspring zwischen 0 und 1
                if event.button == 12:
                    #Code Greifer zu
                    GPIO.output(23, GPIO.HIGH)
                    GPIO.output(17, GPIO.LOW)
                if event.button == 13:
                    #Code Greifer ab; kreis
                    SetAngle(55)
                if event.button == 15:
                    #Code Greifer hoch, quadrat
                    #Testen!
                    SetAngle(120)
                if event.button == 14: 
                    #Code Greifer auf5
                    GPIO.output(23, GPIO.LOW)
                    GPIO.output(17, GPIO.HIGH)
                        
            if event.type==pygame.JOYBUTTONUP:
                print("Button released")
                if event.button == 4 and stick_oder_steuerkreuz == True : #joystick.get_button(13):
                    #buttons_up(a_star)
                    motors_speed(a_star,0,0)
                if event.button == 7 and stick_oder_steuerkreuz == True:
                    #buttons_left(a_star)
                    motors_speed(a_star,0,0)
                if event.button == 5 and stick_oder_steuerkreuz == True:
                    motors_speed(a_star,0,0)
                if event.button == 6 and stick_oder_steuerkreuz == True:   #if joystick.get_button(14): keine fkt!!?
                    motors_speed(a_star,0,0)
                if event.button == 12:
                    #Code Greifer auf stop
                    GPIO.output(23, GPIO.LOW)
                    GPIO.output(17, GPIO.LOW)
                #if event.button == 13:
                    #Code Greifer hoch stop
                    #Kein Code gebraucht, da nur ganz auf oder ganz ab
                #if event.button == 15:
                    #Code Greifer ab stop
                    #Kein Code gebraucht, da nur ganz auf oder ganz ab
                if event.button == 14:
                    #Code Greifer zu stop
                    GPIO.output(23, GPIO.LOW)
                    GPIO.output(17, GPIO.LOW)
                    
except :
    print("Shit was happening")
    GPIO.output(23, GPIO.LOW)
    GPIO.output(17, GPIO.LOW)
    pwm.stop()
    GPIO.cleanup()
 
