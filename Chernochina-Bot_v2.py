#!/usr/bin/python3
from a_star import AStar
import pygame
import RPi.GPIO as GPIO
from time import sleep
import sys
   

def motors_speed(a_star,speed1, speed2):
    a_star.motors(speed1,speed2)

def getAxis(number):
    # when nothing is moved on an axis, the VALUE IS NOT EXACTLY ZERO
    # so this is used not "if joystick value not zero"
    if joystick.get_axis(number) < -0.1 or joystick.get_axis(number) > 0.1:
        # value between 1.0 and -1.0
        print (f"Axis value is {joystick.get_axis(number)}\nAxis ID is {number}")
 
def getButton(number):
    # returns 1 or 0 - pressed or not
    if joystick.get_button(number):
        print (f"Button ID is {number}")

def getHat(number):
    if joystick.get_hat(number) != (0,0):
        # returns tuple with values either 1, 0 or -1
        print (f"Hat value is {joystick.get_hat(number)[0]}, {joystick.get_hat(number)[1]}\nHat ID is {number}")

a_star = AStar()
pygame.init()
pygame.joystick.init()

# how many joysticks connected to computer?
joystick_count = 0

while( joystick_count == 0):
    joystick_count = pygame.joystick.get_count()
    # if no joysticks, quit program safely

    # initialise joystick
joystick = pygame.joystick.Joystick(0)
joystick.init()

while True:
    try:
        for event in pygame.event.get():
            left_stick = float(joystick.get_axis(1))
            right_stick = float(joystick.get_axis(3))
            a_star.motors(int(200*left_stick),int(200*right_stick))
            sleep(0.002)

    except :
        a_star.motors(int(0),int(0))
        print("Programm beendet")